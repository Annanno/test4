package com.example.test4

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.test4.databinding.ItemLayoutBinding

typealias ClickItem = (item: Item, position: Int) -> Unit

var playerFirst = true

class Adapter : RecyclerView.Adapter<Adapter.ViewHolder>() {
    private val items = mutableListOf<Item>()
    var callback: ClickItem? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false))
    }

    override fun getItemCount() = items.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }


    inner class ViewHolder(private val binding: ItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root),
        View.OnClickListener {
        private lateinit var model: Item

        fun onBind() {
            model = items[adapterPosition]
            binding.root.setOnClickListener(this)

        }

        private fun setResource() {
            if (playerFirst) {
                binding.item.setImageResource(R.drawable.x)
                model.icon = "x"
            } else {
                binding.item.setImageResource(R.drawable.zero)
                model.icon = "0"
            }
            playerFirst = !playerFirst

        }

        override fun onClick(v: View?) {
            callback?.invoke(model, adapterPosition)
            setResource()
            v?.isClickable = false

        }

    }

    fun setData(mutableList: MutableList<Item>) {
        this.items.clear()
        this.items.addAll(mutableList)
        notifyDataSetChanged()
    }

//    fun checkWinner3(){
//        for (i in winPositions3){
//            if (items[i[0]].btnName == items[i[1]].btnName && items[i[1]].btnName == items[i[2]].btnName)
//                Toast.makeText(, "winner is ${items[i[0]].btnName}", Toast.LENGTH_SHORT).show()
//
//        }
//
//    }


}








