package com.example.test4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.example.test4.databinding.ActivityGameBinding

class GameActivity : AppCompatActivity() {
    private lateinit var binding: ActivityGameBinding
    private lateinit var adapter: Adapter
    private var items = mutableListOf<Item>()
    val winPositions3 = arrayOf(
        intArrayOf(0, 1, 2),
        intArrayOf(3, 4, 5),
        intArrayOf(6, 7, 8),
        intArrayOf(0, 3, 6),
        intArrayOf(1, 4, 7),
        intArrayOf(2, 5, 8),
        intArrayOf(0, 4, 8),
        intArrayOf(2, 4, 6)

    )
    val winPositions4 = arrayOf(
        intArrayOf(0, 1, 2, 3),
        intArrayOf(4, 5, 6, 7),
        intArrayOf(8, 9, 10, 11),
        intArrayOf(12, 13, 14, 15),
        intArrayOf(0, 5, 10, 15),
        intArrayOf(3, 6, 9, 12),
        intArrayOf(0, 4, 8, 12),
        intArrayOf(1, 5, 9, 13),
        intArrayOf(2, 6, 10, 14),
        intArrayOf(3, 7, 11, 15)

    )
    val winPositions5 = arrayOf(
        intArrayOf(0, 1, 2, 3, 4),
        intArrayOf(5, 6, 7, 8, 9),
        intArrayOf(10, 11, 12, 13, 14),
        intArrayOf(15, 16, 17, 18, 19),
        intArrayOf(20, 21, 22, 23, 24),
        intArrayOf(0, 5, 10, 15, 20),
        intArrayOf(1, 6, 11, 16, 21),
        intArrayOf(2, 7, 12, 17, 22),
        intArrayOf(3, 8, 13, 18, 23),
        intArrayOf(4, 9, 14, 19, 24),
        intArrayOf(0, 6, 12, 18, 24),
        intArrayOf(4, 8, 12, 16, 10),

        )


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGameBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }


    fun init() {
        addItems()
        val size = intent.getIntExtra("size", 9)
        binding.recyclerView.layoutManager = GridLayoutManager(this, size)
        adapter = Adapter()
        binding.recyclerView.adapter = adapter
        adapter.callback = { item, position ->
            if (items.size == 9)
                checkWinner3()
            else if (items.size == 16)
                checkWinner4()
            else if (items.size == 25)
                checkWinner5()
        }

        adapter.setData(items)

    }

    fun addItems() {
        val size = intent.getIntExtra("size", 3)
        for (i in 0 until size * size)
            items.add(Item("button${i}"))

    }

    fun checkWinner3() {
        val intent = Intent(this, ResultActivity::class.java)
        for (i in winPositions3) {
            if (items[i[0]].icon == items[i[1]].icon && items[i[1]].icon == items[i[2]].icon) {
                Toast.makeText(this, "winner is ${items[i[0]].icon}", Toast.LENGTH_SHORT).show()
                intent.putExtra("result", items[i[0]].icon)
                startActivity(intent)
            }
        }
    }

    fun checkWinner4() {
        val intent = Intent(this, ResultActivity::class.java)
        for (i in winPositions4) {
            if (items[i[0]].icon == items[i[1]].icon && items[i[1]].icon == items[i[2]].icon &&
                items[i[2]].icon == items[i[3]].icon
            ) {
                Toast.makeText(this, "winner is ${items[i[0]].icon}", Toast.LENGTH_SHORT).show()
                intent.putExtra("result", items[i[0]].icon)
                startActivity(intent)
            }

        }
    }

    fun checkWinner5() {
        val intent = Intent(this, ResultActivity::class.java)
        for (i in winPositions5) {
            if (items[i[0]].icon == items[i[1]].icon && items[i[1]].icon == items[i[2]].icon &&
                items[i[2]].icon == items[i[3]].icon && items[i[3]].icon == items[i[4]].icon
            ) {
                Toast.makeText(this, "winner is ${items[i[0]].icon}", Toast.LENGTH_SHORT).show()
                intent.putExtra("result", items[i[0]].icon)
                startActivity(intent)
            }
        }


    }
}







