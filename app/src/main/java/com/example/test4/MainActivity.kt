package com.example.test4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.test4.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()

    }

    private fun init() {
        val number = listOf<Int>(3, 4, 5)
        binding.startGame.setOnClickListener {
            val size = binding.enterNumber.text.toString().toInt()
            val intent = Intent(this, GameActivity::class.java)
            intent.putExtra("size", size)
            if (size in number)
                startActivity(intent)
            else Toast.makeText(this, "enter 3, 4 or 5", Toast.LENGTH_SHORT).show()

        }
    }
}