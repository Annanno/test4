package com.example.test4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.test4.databinding.ActivityResultBinding

class ResultActivity : AppCompatActivity() {
    private lateinit var binding: ActivityResultBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityResultBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    fun init(){
        val winner = intent.getStringExtra("result")
        binding.result.text = "Winner is ${winner}"
    }
}